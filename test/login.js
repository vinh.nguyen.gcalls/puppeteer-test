const puppeteer = require('puppeteer');
const expect = require('chai').expect;

describe('Test login page', () => {
    let browser;
    let page;

    before(async () => {
        browser = await puppeteer.launch({
            headless: false,
            slowMo: 10,
            devtools: false,
            defaultViewport: null
        });
        page = await browser.newPage();
        await page.setDefaultTimeout(10000);
        await page.setDefaultNavigationTimeout(20000);
    }
    );

    beforeEach(async () => {
        // Before each test we open a new page.
        page = await browser.newPage();
        await page.goto('https://app.gcalls.co/');
    }
    );

    it('Enter page successfully', async () => {
        const title = await page.title();
        expect(title).to.equal("Gcalls - Đăng nhập");
    });

    it('Enter empty tenant', async () => {
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#tenantError").innerText === "Tên tổng đài không được rỗng"');
    });

    it('Login with incorrect callcenter', async () => {
        await page.type('#tenant', 'callcenter');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#tenantError").innerText === "Không tìm thấy tổng đài"');
    });

    it('Login with correct callcenter', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();

        const title = await page.title();
        expect(title).to.match(/^Sign in to (.+)$/);
    });

    it('Login with email does not exist', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();
        await page.type('#username', 'nguyenphucvinh@gmail.com');
        await page.type('#password', '123456');
        await page.click("input[type='submit']");
        await page.waitForFunction('document.querySelector("#input-error").innerText === "Invalid username or password."');
    });

    it('Login with correct email and empty password', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();
        await page.type('#username', 'nguyenphucvinh9x@gmail.com');
        await page.click("input[type='submit']");
        await page.waitForFunction('document.querySelector("#input-error").innerText === "Invalid username or password."');
    });

    it('Login with email invalid local part', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();
        await page.type('#username', '@gmail.com');
        await page.type('#password', '123456');
        await page.click("input[type='submit']");
        await page.waitForFunction('document.querySelector("#input-error").innerText === "Invalid username or password."');
    });

    it('Login with email invalid domain part', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();
        await page.type('#username', 'nguyenphucvinh9x@');
        await page.type('#password', '123456');
        await page.click("input[type='submit']");
        await page.waitForFunction('document.querySelector("#input-error").innerText === "Invalid username or password."');
    });

    it('Login with email invalid domain part without .', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();
        await page.type('#username', 'nguyenphucvinh9x@gmail');
        await page.type('#password', '123456');
        await page.click("input[type='submit']");
        await page.waitForFunction('document.querySelector("#input-error").innerText === "Invalid username or password."');
    });

    it('Login with email invalid without @', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();
        await page.type('#username', 'nguyenphucvinh9xgmail.com');
        await page.type('#password', '123456');
        await page.click("input[type='submit']");
        await page.waitForFunction('document.querySelector("#input-error").innerText === "Invalid username or password."');
    });

    it('Login with correct email and incorrect password', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();
        await page.type('#username', 'nguyenphucvinh9x@gmail.com');
        await page.type('#password', '123456');
        await page.click("input[type='submit']");
        await page.waitForFunction('document.querySelector("#input-error").innerText === "Invalid username or password."');

        // const result = await page.evaluate(() => {
        //     return document.querySelector("#input-error").innerText;
        // });

        // expect(result).to.equal("Invalid username or password.");
    });

    it('Login successfully', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();
        await page.type('#username', 'nguyenphucvinh9x@gmail.com');
        await page.type('#password', 'phucvinh39');
        await page.click("input[type='submit']");
        await page.waitForNavigation();
        const title = await page.title();

        expect(title).to.equal('Gcalls - Trang chủ');
    });

    afterEach(async () => {
        await page.close();
    });

    after(async () => {
        await browser.close();
    }
    );
});