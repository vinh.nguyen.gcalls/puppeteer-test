const puppeteer = require('puppeteer');
const expect = require('chai').expect;

describe('Test signup page', () => {
    let browser;
    let page;

    before(async () => {
        browser = await puppeteer.launch({
            headless: false,
            slowMo: 10,
            devtools: false,
            defaultViewport: null
        });
        page = await browser.newPage();
        await page.setDefaultTimeout(10000);
        await page.setDefaultNavigationTimeout(20000);

    }
    );

    beforeEach(async () => {
        // Before each test we open a new page.
        page = await browser.newPage();
        await page.setViewport({ width: 1920, height: 1080 });
        await page.goto('https://app.gcalls.co/');
    }
    );

    it('Enter page successfully', async () => {
        await page.click("a[href='/g/signup']");
        await page.waitForSelector('.swal-button--confirm');
        await page.click('.swal-button--confirm');
        const title = await page.title();
        expect(title).to.equal('Gcalls - Đăng ký');
    });

    it('Enter empty tenant and email', async () => {
        await page.click("a[href='/g/signup']");
        await page.waitForSelector('.swal-button--confirm');
        await page.click('.swal-button--confirm');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#tenantError").innerText == "Link tổng đài không được rỗng"');
        await page.waitForFunction('document.querySelector("#emailError").innerText == "Email Admin không được rỗng"');
    });

    it('Enter empty tenant', async () => {
        await page.click("a[href='/g/signup']");
        await page.waitForSelector('.swal-button--confirm');
        await page.click('.swal-button--confirm');
        await page.type('#email', 'vinhnguyen03091999@gmail.com');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#tenantError").innerText == "Link tổng đài không được rỗng"');
    });

    it('Enter empty email', async () => {
        await page.click("a[href='/g/signup']");
        await page.waitForSelector('.swal-button--confirm');
        await page.click('.swal-button--confirm');
        await page.type('#tenant', 'vinhnguyen-test');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#emailError").innerText == "Email Admin không được rỗng"');
    });

    it('Enter email without Local part', async () => {
        await page.click("a[href='/g/signup']");
        await page.waitForSelector('.swal-button--confirm');
        await page.click('.swal-button--confirm');
        await page.type('#tenant', 'vinhnguyen-test');
        await page.type('#email', '@gmail.com');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#emailError").innerText == "Email Admin không đúng định dạng"');
    });

    it('Enter email without @ part', async () => {
        await page.click("a[href='/g/signup']");
        await page.waitForSelector('.swal-button--confirm');
        await page.click('.swal-button--confirm');
        await page.type('#tenant', 'vinhnguyen-test');
        await page.type('#email', 'vinhnguyen03091999gmail.com');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#emailError").innerText == "Email Admin không đúng định dạng"');
    });

    it('Enter email without Domain part', async () => {
        await page.click("a[href='/g/signup']");
        await page.waitForSelector('.swal-button--confirm');
        await page.click('.swal-button--confirm');
        await page.type('#tenant', 'vinhnguyen-test');
        await page.type('#email', 'vinhnguyen03091999@');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#emailError").innerText == "Email Admin không đúng định dạng"');
    });

    it('Enter email with invalid domain part', async () => {
        await page.click("a[href='/g/signup']");
        await page.waitForSelector('.swal-button--confirm');
        await page.click('.swal-button--confirm');
        await page.type('#tenant', 'vinhnguyen-test');
        await page.type('#email', 'vinhnguyen03091999@gmail');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#emailError").innerText == "Email Admin không đúng định dạng"');
    });

    it('Enter email without dot part in domain part', async () => {
        await page.click("a[href='/g/signup']");
        await page.waitForSelector('.swal-button--confirm');
        await page.click('.swal-button--confirm');
        await page.type('#tenant', 'vinhnguyen-test');
        await page.type('#email', 'vinhnguyen03091999@gmailcom');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#emailError").innerText == "Email Admin không đúng định dạng"');
    });

    // it('Enter invalid tenant', async () => {
    //     await page.click("a[href='/g/signup']");
    //     await page.waitForSelector('.swal-button--confirm');
    //     await page.click('.swal-button--confirm');
    //     await page.type('#tenant', 'vinhnguyen-test@');
    //     await page.type('#email', 'vinhnguyen03091999@gmail');
    //     await page.click("button[type='submit']");
    //     await page.waitForFunction('document.querySelector("#tenantError").innerText == "Link tổng đài không đúng định dạng"');
    // });

    it('Enter tenant has been already existed', async () => {
        await page.click("a[href='/g/signup']");
        await page.waitForSelector('.swal-button--confirm');
        await page.click('.swal-button--confirm');
        await page.type('#tenant', 'vinh-test123');
        await page.type('#email', 'nguyenphucvinh9x@gmail.com');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#tenantError").innerText == "Tổng đài đã tồn tạ"');
    });


    it('Enter valid tenant and email', async () => {
        await page.click("a[href='/g/signup']");
        await page.waitForSelector('.swal-button--confirm');
        await page.click('.swal-button--confirm');
        await page.type('#tenant', 'vinhnguyen-test');
        await page.type('#email', 'vinhnguyen03091999@gmail.com');
        await page.click("button[type='submit']");
        await page.waitForSelector('.modal-body');
        await page.waitForFunction('document.querySelector(".modal-body h6").innerText == "Kiểm tra email trong vòng 48h để thực hiện các bước tiếp theo của thủ tục đăng ký tài khoản"');
    });

    afterEach(async () => {
        await page.close();
    });

    after(async () =>
        await browser.close()
    );
});