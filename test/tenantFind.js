const puppeteer = require('puppeteer');
const expect = require('chai').expect;

describe('Test tenant find', () => {

    let browser;
    let page;

    before(async () => {
        browser = await puppeteer.launch({
            headless: false,
            slowMo: 10,
            devtools: false,
            defaultViewport: null
        });
        page = await browser.newPage();
        await page.setDefaultTimeout(10000);
        await page.setDefaultNavigationTimeout(20000);

    });

    beforeEach(async () => {
        // Before each test we open a new page.
        page = await browser.newPage();
        // await page.setViewport({ width: 1920, height: 1080 });
        await page.goto('https://app.gcalls.co/');
    });

    it('Enter page successfully', async () => {
        await page.click("a[href='/g/forgetcallcentername']");
        const title = await page.title();
        expect(title).to.equal('Gcalls - Bạn không nhớ tên tổng đài? Bấm vào đây!');
    });

    it('Enter empty email', async () => {
        await page.click("a[href='/g/forgetcallcentername']");
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#emailError").innerText == "Email không được rỗng"');
    });

    it('Enter invalid email', async () => {
        await page.click("a[href='/g/forgetcallcentername']");
        await page.type('#email', 'nguyenphucvinh9x');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#emailError").innerText == "Email không đúng định dạng"');
    });

    it('Enter not exist email', async () => {
        await page.click("a[href='/g/forgetcallcentername']");
        await page.type('#email', 'nguyenphucvinh8x@gmail.com');
        await page.click("button[type='submit']");
        await page.waitForSelector('.modal-content');
        await page.waitForFunction('document.querySelector(".modal-body h5").innerText == "Email chưa được đăng ký"');
    });

    it('Enter exist email', async () => {
        await page.click("a[href='/g/forgetcallcentername']");
        await page.type('#email', 'nguyenphucvinh9x@gmail.com');
        await page.click("button[type='submit']");
        await page.waitForSelector('.modal-content');
        await page.waitForFunction('document.querySelector(".modal-body h5").innerText == "Email đã được gửi"');
    });

    afterEach(async () => {
        await page.close();
    });

    after(async () => {
        await browser.close();
    });

});