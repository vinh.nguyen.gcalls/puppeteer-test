const puppeteer = require('puppeteer');
const expect = require('chai').expect;

describe('Test forgot password page', () => {

    let browser;
    let page;

    before(async () => {
        browser = await puppeteer.launch({
            headless: false,
            slowMo: 10,
            devtools: false,
            defaultViewport: null
        });
        page = await browser.newPage();
        await page.setDefaultTimeout(10000);
        await page.setDefaultNavigationTimeout(20000);
    });

    beforeEach(async () => {
        // Before each test we open a new page.
        page = await browser.newPage();
        // await page.setViewport({ width: 1920, height: 1080 });
        await page.goto('https://app.gcalls.co/');
    });

    it('Enter page successfully', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();
        await page.click("a[tabindex='5']");
        const title = await page.title();
        expect(title).to.match(/^Sign in to (.+)$/);
    });

    it('Enter empty email', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();
        await page.click("a[tabindex='5']");
        await page.click("input[type='submit']");
        await page.waitForFunction('document.querySelector("#input-error-username").innerText == "Please specify username."');
    });

    it('Enter invalid email', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();
        await page.click("a[tabindex='5']");
        await page.type('#username', 'nguyenphucvinh8x');
        await page.click("input[type='submit']");
        await page.waitForFunction('document.querySelector("#input-error-username").innerText == "Please enter a valid email address."');
    });

    it('Enter valid email', async () => {
        await page.type('#tenant', 'vinh-test123');
        await page.click("button[type='submit']");
        await page.waitForNavigation();
        await page.click("a[tabindex='5']");
        await page.type('#username', 'nguyenphucvinh9x@gmail.com');
        await page.click("input[type='submit']");
        await page.waitForFunction('document.querySelector(".alert-success span.kc-feedback-text").innerText == "You should receive an email shortly with further instructions."');
    });

    afterEach(async () => {
        await page.close();
    });

    after(async () => {
        await browser.close();
    });

});